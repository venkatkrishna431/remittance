package com.remittance.db;

import com.remittance.dao.AccountDetailsDAO;
import com.remittance.dao.TransferDetailsDAO;

public interface DataAccessObjectBuilder {

	AccountDetailsDAO getAccountDetailsDAO();
	TransferDetailsDAO getTransferDetailsDAO();
	void populateTestData(String fileName);
	public static DataAccessObjectBuilder getH2Object() {
		return new DataAccessObject();
	}
	void truncateTestedData(String dropScript);
}
